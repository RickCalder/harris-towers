<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://calder.io
 * @since      1.0.0
 *
 * @package    Wise_Login
 * @subpackage Wise_Login/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    Wise_Login
 * @subpackage Wise_Login/public
 * @author     calder.io info@calder.io
 */
class Wise_Login_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $wise_login    The ID of this plugin.
	 */
	private $wise_login;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $wise_login       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $wise_login, $version ) {

		$this->wise_login = $wise_login;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wise_Login_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wise_Login_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->wise_login, plugin_dir_url( __FILE__ ) . 'css/wise-login-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wise_Login_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wise_Login_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->wise_login, plugin_dir_url( __FILE__ ) . 'js/wise-login-public.js', array( 'jquery' ), $this->version, false );

	}

}
