window.onload = placeholders;

function placeholders() {
  var l = document.getElementById("user_login")
  if (l) l.placeholder = "Username";
  var p = document.getElementById("user_pass")
  if (p) p.placeholder = "Password";
  var labels = document.getElementsByTagName('LABEL')
  for (var i = 0; i < labels.length; i++) {
    if (labels[i].htmlFor != '') {
        var elem = document.getElementById(labels[i].htmlFor)
        if ( labels[i].htmlFor === 'user_login') {
          labels[i].innerHTML = 'Username<br><input type="text" name="log" id="user_login" class="input" size="20" placeholder="Username">'
        }
             
    }
  }
  var newDiv = document.createElement('div')
  if( getWidth() > 768 ) {
    newDiv.style.width = "50%"
    newDiv.style.margin = '100px auto 0'
  } else {
    newDiv.style.width = '90%'
    newDiv.style.margin = '50px auto 0'
  }
  newDiv.style.fontSize = "1rem"
  document.getElementById('login').style.paddingTop = '2%'
  newDiv.innerHTML = '<h2 style="margin-bottom: 15px; text-align: center; line-height: 1.4">Welcome to your new Harris Towers website</h2><p style="margin-bottom: 15px; font-size: 1rem">Your login credentials from the old website still work here, if you have forgotten those credentials please contact Bill at <a href="mailto:calder408@gmail.com">calder408@gmail.com</a>. Make sure to include your name, building and unit number in the email!</p>'
  document.body.insertBefore(newDiv, document.body.firstChild)
  console.log(newDiv)
  document.getElementById('login').style.display = 'block'
}

function getWidth() {
  return Math.max(
    document.body.scrollWidth,
    document.documentElement.scrollWidth,
    document.body.offsetWidth,
    document.documentElement.offsetWidth,
    document.documentElement.clientWidth
  );
}