<?php

/**
 * The dashboard-specific functionality of the plugin.
 *
 * @link       https://calder.io
 * @since      1.0.0
 *
 * @package    Wise_Login
 * @subpackage Wise_Login/admin
 */

/**
 * The dashboard-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    Wise_Login
 * @subpackage Wise_Login/admin
 * @author     calder.io info@calder.io
 */
class Wise_Login_Admin {

  /**
   * The ID of this plugin.
   *
   * @since    1.0.0
   * @access   private
   * @var      string    $wise_login    The ID of this plugin.
   */
  private $wise_login;

  /**
   * The version of this plugin.
   *
   * @since    1.0.0
   * @access   private
   * @var      string    $version    The current version of this plugin.
   */
  private $version;

  /**
   * Initialize the class and set its properties.
   *
   * @since    1.0.0
   * @param      string    $wise_login       The name of this plugin.
   * @param      string    $version    The version of this plugin.
   */
  public function __construct( $wise_login, $version ) {

    $this->wise_login = $wise_login;
    $this->version = $version;

    if (function_exists('acf_add_options_page') && function_exists('acf_add_local_field_group')) {
      acf_add_options_page('Enhanced Login');
      acf_add_local_field_group(array (
        'key' => 'group_56c33ae3d9565',
        'title' => 'Enhanced Login Branding',
        'fields' => array (
          array (
            'key' => 'field_56c33df862076',
            'label' => 'Logo',
            'name' => 'enhanced_wordpress_login_logo',
            'type' => 'image',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
              'width' => '',
              'class' => '',
              'id' => '',
            ),
            'return_format' => 'url',
            'preview_size' => 'thumbnail',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
          ),
        ),
        'location' => array (
          array (
            array (
              'param' => 'options_page',
              'operator' => '==',
              'value' => 'acf-options-enhanced-login',
            ),
          ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
      ));
    }
  }

  /**
   * Register the stylesheets for the Dashboard.
   *
   * @since    1.0.0
   */
  public function enqueue_styles() {

    /**
     * This function is provided for demonstration purposes only.
     *
     * An instance of this class should be passed to the run() function
     * defined in Wise_Login_Loader as all of the hooks are defined
     * in that particular class.
     *
     * The Wise_Login_Loader will then create the relationship
     * between the defined hooks and the functions defined in this
     * class.
     */

    wp_enqueue_style( $this->wise_login, plugin_dir_url( __FILE__ ) . 'css/wise-login-admin.css', array(), $this->version, 'all' );

  }

  /**
   * Register the JavaScript for the dashboard.
   *
   * @since    1.0.0
   */
  public function enqueue_scripts() {

    /**
     * This function is provided for demonstration purposes only.
     *
     * An instance of this class should be passed to the run() function
     * defined in Wise_Login_Loader as all of the hooks are defined
     * in that particular class.
     *
     * The Wise_Login_Loader will then create the relationship
     * between the defined hooks and the functions defined in this
     * class.
     */

    wp_enqueue_script( $this->wise_login, plugin_dir_url( __FILE__ ) . 'js/wise-login-admin.js', array( 'jquery' ), $this->version, false );

  }

}
