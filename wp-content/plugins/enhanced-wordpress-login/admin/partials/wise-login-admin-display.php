<?php

/**
 * Provide a dashboard view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://calder.io
 * @since      1.0.0
 *
 * @package    Wise_Login
 * @subpackage Wise_Login/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
