<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the dashboard.
 *
 * @link       https://calder.io
 * @since      1.0.0
 *
 * @package    Wise_Login
 * @subpackage Wise_Login/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, dashboard-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Wise_Login
 * @subpackage Wise_Login/includes
 * @author     calder.io info@calder.io
 */
class Wise_Login {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Wise_Login_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $wise_login    The string used to uniquely identify this plugin.
	 */
	protected $wise_login;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the Dashboard and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->wise_login = 'wise-login';
		$this->version = '1.0.0';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		//$this->define_public_hooks();

		$this->transform_login();
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Wise_Login_Loader. Orchestrates the hooks of the plugin.
	 * - Wise_Login_i18n. Defines internationalization functionality.
	 * - Wise_Login_Admin. Defines all hooks for the dashboard.
	 * - Wise_Login_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wise-login-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wise-login-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the Dashboard.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-wise-login-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-wise-login-public.php';

		/**
		 * These classes are responsible for defining new post types.
		 */
		foreach (glob(plugin_dir_path( dirname( __FILE__ ) ) . "includes/post-types/*") as $filename) {
			require_once $filename;
		}

		$this->loader = new Wise_Login_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Wise_Login_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Wise_Login_i18n();
		$plugin_i18n->set_domain( $this->get_wise_login() );

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the dashboard functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Wise_Login_Admin( $this->get_wise_login(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Wise_Login_Public( $this->get_wise_login(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_wise_login() {
		return $this->wise_login;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Wise_Login_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	/**
	 * Transform the login screen.
	 */
	private function transform_login() {
		add_action("login_enqueue_scripts", array($this, 'login_enqueue_scripts'));
		add_action("init", array($this, 'test_login_form'));
    add_action('login_head', array($this, 'custom_logo_style'));
	}

	/**
	 * Enqueue login stylesheets and scripts
	 */
	public function login_enqueue_scripts() {
		wp_enqueue_style('custom-login', plugins_url() . '/enhanced-wordpress-login/public/css/login.css');
		wp_enqueue_script('custom-login', plugins_url() . '/enhanced-wordpress-login/public/js/login.js');
	}

	/**
	 * Test login form for visibility
	 */
	public function test_login_form() {
		global $pagenow;
		if ($pagenow === "wp-login.php") {
			add_filter("gettext", array($this, 'login_form_text'));
		}
	}

  public function custom_logo_style() {
    if (!function_exists('get_field')) {
      return;
    }

    $logo_path = get_field('enhanced_wordpress_login_logo', 'options');
    if (!$logo_path) {
      return;
    }

    echo '<style>';
    echo '.login h1 a {';
    echo 'background-image: url(' . $logo_path . ') !important; background-repeat: no-repeat; background-position: center center;';
    echo '}</style>';
  }

	/**
	 * Clobber text in the login form
	 */
	public function login_form_text($translated_text, $text = '', $domain = '') {
		if ($translated_text === "Username" || $translated_text === "Password") {
			$translated_text = "";
		}
		return $translated_text;
	}
}
