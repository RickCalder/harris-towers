<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * Dashboard. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://calder.io
 * @since             1.0.0
 * @package           Wise_Login
 *
 * @wordpress-plugin
 * Plugin Name:       Enhanced WordPress Login
 * Description:       Enhance the WordPress authentication experience
 * Version:           1.0.0
 * Author:            calder.io
 * Author URI:        https://calder.io
 * License:           Copyright 2018 calder.io All rights reserved.
 * Text Domain:       wise-login
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wise-login-activator.php
 */
function activate_wise_login() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wise-login-activator.php';
	Wise_Login_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wise-login-deactivator.php
 */
function deactivate_wise_login() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wise-login-deactivator.php';
	Wise_Login_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wise_login' );
register_deactivation_hook( __FILE__, 'deactivate_wise_login' );

/**
 * The core plugin class that is used to define internationalization,
 * dashboard-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wise-login.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wise_login() {

	$plugin = new Wise_Login();
	$plugin->run();

}
run_wise_login();
