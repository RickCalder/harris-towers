# Enhanced WordPress Login

Enhance the WordPress authentication experience

## Changelog

* As of 7f3f2b0, this includes a custom field and options page if Advanced Custom Fields exists. This field specifies a custom logo and should use an image with dimensions of 120x120 pixels.
