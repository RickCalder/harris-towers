<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

if(! is_user_logged_in() ) {
  header('Location: /');
}

$templates = array( 'pages/page-announcements.twig' );
// die;
$context = Timber::get_context();


// print_r($templates);die;

global $paged;
if ( ! isset( $paged ) || ! $paged ) {
  $paged = 1;
}


$args    = [
  'post_type'     => 'announcements',
  'posts_per_page' => 12,
  'paged'          => $paged,
];

$context['posts'] = new Timber\PostQuery( $args );
$context['pagination'] = Timber::get_pagination();

Timber::render( $templates, $context );
