<?php
/* File: page.php */
$context         = Timber::get_context();
$context['post'] = Timber::query_post();
$templates = array('pages/page-'.$post->post_name.'.twig', 'pages/page.twig');


if( is_page('galleries') && !is_user_logged_in() ) {
  header('Location: /');
}

if( is_page('pics') && !is_user_logged_in() ) {
  header('Location: /');
}

if( is_page('approved-contractors') && !is_user_logged_in() ) {
  header('Location: /');
}

if( is_page('events') && !is_user_logged_in() ) {
  header('Location: /');
}

if( is_page('owners') && !is_user_logged_in() ) {
  header('Location: /');
}

if( is_front_page() && ! is_user_logged_in() ) {
  array_unshift($templates, 'pages/front.twig');
}

if( (is_front_page() || is_page('owners')) && is_user_logged_in() ) {
  array_unshift($templates, 'pages/owner-home.twig');
  $args    = [
    'post_type'      => 'foogallery',
    'posts_per_page' => 3,
    'paged'          => $paged,
  ];
  $context['galleries'] = new \Timber\PostQuery( $args );

  
  $args    = [
    'post_type'      => 'announcements',
    'posts_per_page' => 3,
    'paged'          => $paged,
  ];
  $context['announcements'] = new \Timber\PostQuery( $args );

  $args    = [
    'post_type'      => 'post',
    'category_name'  => 'newsletter',
    'posts_per_page' => 3,
    'paged'          => $paged,
  ];
  $context['newsletters'] = new \Timber\PostQuery( $args );

  $args    = [
    'post_type'      => 'tribe_events',
    'posts_per_page' => 3,
    'paged'          => $paged,
  ];
  $context['events'] = new \Timber\PostQuery( $args );
  // echo '<xmp>';print_r($context['events']);die;

}

if( is_page('galleries') && is_user_logged_in() ) {
  global $paged;
  if ( ! isset( $paged ) || ! $paged ) {
    $paged = 1;
  }
  $args    = [
    'post_type'      => 'foogallery',
    'posts_per_page' => 3,
    'paged'          => $paged,
  ];


  $context['posts']      = new \Timber\PostQuery( $args );
  // $context['pagination'] = Timber::get_pagination();
  // echo '<xmp>';print_r($context['pagination']);die;
  $templates             = [
    'pages/page-galleries.twig',
  ];
}

if( is_user_logged_in() && ! is_page('announcements') ) {
  $args = array (
    'post_type' => 'announcements',
    'meta_key' => 'featured',
    'meta_value' => true,
    'posts_per_page' => 1
  );
  $featured_announcement = Timber::get_posts( $args );
  $context['featured_announcement'] = $featured_announcement[0];
}

if( is_page( 'approved-contractors' ) ) {
  $args = array (
    'post_type' => 'contractors',
    'posts_per_page' => -1
  );
  $context['contractors'] = Timber::get_posts( $args );
}

// echo '<xmp>'; print_r($context['post']);die;

Timber::render($templates, $context);