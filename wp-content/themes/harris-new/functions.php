<?php

add_theme_support('menus');
add_theme_support('post-thumbnails');


add_filter('timber_context', function($data) {
  $data['menu'] = new TimberMenu('header-menu');
  $data['owner_menu'] = new TimberMenu('owner-menu');
  // echo '<xmp>';print_r($data['owner_menu']); die;
  $data['options'] = get_fields('options');
  $data['before_content'] = Timber::get_widgets('before_content');
  $data['after_content'] = Timber::get_widgets('after_content');
  $data['page_sidebar'] = Timber::get_widgets('page_sidebar');
  $data['post_sidebar'] = Timber::get_widgets('post_sidebar');
  $data['logout_url'] = wp_logout_url( home_url() );

  return $data;
});



// Register Custom Post Types
add_action('init', function() {
  $announcement_labels = array(
    'name'                => __( 'Announcements' ),
    'singular_name'       => __( 'Announcement' ),
    'menu_name'           => __( 'Announcements' ),
    'add_new_item'        => __( ' Add New Announcement' ),
    'all_items'           => __( 'All Announcements' ),
    'view_item'           => __( 'View Announcement' ),
    'add_new_item'        => __( 'Add New Announcement' ),
    'add_new'             => __( 'Add New' ),
    'edit_item'           => __( 'Edit Announcement' ),
    'update_item'         => __( 'Update Announcement' ),
    'search_items'        => __( 'Search Announcements' ),
    'not_found'           => __( 'Not Found' ),
    'not_found_in_trash'  => __( 'Not found in Trash' ),
  );
  $announcement_args = array(
    'public'             => true,
    'show_in_rest'       => true,
    'labels'             => $announcement_labels,
    'public'             => true,
    'capability_type'    => 'post',
    'has_archive'        => true,
    'supports'           => array( 'title', 'editor', 'revisions' ),
    'menu_icon'         =>  'dashicons-megaphone'
  );

  
  $contractor_labels = array(
    'name'                => __( 'Approved Contractors' ),
    'singular_name'       => __( 'Approved Contractor' ),
    'menu_name'           => __( 'Approved Contractors' ),
    'add_new_item'        => __( ' Add New Announcement' ),
    'all_items'           => __( 'All Approved Contractors' ),
    'view_item'           => __( 'View Approved Contractor' ),
    'add_new_item'        => __( 'Add New Approved Contractor' ),
    'add_new'             => __( 'Add New' ),
    'edit_item'           => __( 'Edit Approved Contractor' ),
    'update_item'         => __( 'Update Approved Contractor' ),
    'search_items'        => __( 'Search Approved Contractors' ),
    'not_found'           => __( 'Not Found' ),
    'not_found_in_trash'  => __( 'Not found in Trash' ),
  );
  $contractor_args = array(
    'public'             => true,
    'show_in_rest'       => true,
    'labels'             => $contractor_labels,
    'public'             => true,
    'capability_type'    => 'post',
    'has_archive'        => true,
    'supports'           => array( 'title' ),
    'menu_icon'         =>  'dashicons-admin-tools'
  );
  
  register_post_type('announcements', $announcement_args);
  register_post_type('contractors', $contractor_args);
});


if (function_exists('acf_add_options_page')) {
  // acf_add_options_page('Site Info');
	// acf_add_options_sub_page('Board Members');
  // acf_add_options_sub_page('Superintendent');
  
  acf_add_options_page(array(
		'page_title' 	=> 'Site Info',
		'menu_title'	=> 'Site Info',
		'menu_slug' 	=> 'site-info',
		'capability'	=> 'edit_posts',
		'redirect'		=> true
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Board Members',
		'menu_title'	=> 'Board Members',
		'parent_slug'	=> 'site-info',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Social Committee',
		'menu_title'	=> 'Social Committee',
		'parent_slug'	=> 'site-info',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Superintendent',
		'menu_title'	=> 'Superintendent',
		'parent_slug'	=> 'site-info',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Property Manager',
		'menu_title'	=> 'Property Manager',
		'parent_slug'	=> 'site-info',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Contractor Rules',
		'menu_title'	=> 'Contractor Rules',
		'parent_slug'	=> 'site-info',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Front Slider',
		'menu_title'	=> 'Front Slider',
		'parent_slug'	=> 'site-info',
	));
}

add_image_size( 'slider', 1200, 9999 );
function asset_path($path) {
  return get_stylesheet_directory_uri() . '/assets/dist/' . $path;
}


//remove WP Emoji

function disable_wp_emojicons() {

  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

  // filter to remove TinyMCE emojis
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );

function disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}


//Hide admin bar for users
add_action('set_current_user', 'cc_hide_admin_bar');
function cc_hide_admin_bar() {
  if (!current_user_can('edit_posts')) {
    show_admin_bar(false);
  }
}

/**
 * Redirect non-admins to the homepage after logging into the site.
 *
 * @since 	1.0
 */
function harris_login_redirect( $redirect_to, $request, $user  ) {
	return ( is_array( $user->roles ) && ( in_array( 'administrator', $user->roles) || in_array('contributor', $user->roles) ) ) ? admin_url() : site_url();
}
add_filter( 'login_redirect', 'harris_login_redirect', 10, 3 );

add_action( 'admin_init', 'redirect_non_admin_users' );
/**
 * Redirect non-admin users to home page
 *
 * This function is attached to the 'admin_init' action hook.
 */
// function redirect_non_admin_users() {
//   $user = wp_get_current_user();
// 	if ( (  DOING_AJAX ) || ( in_array( 'administrator', $user->roles) || in_array('contributor', $user->roles) )  ) {
// 	} else {
//     wp_redirect( home_url() );
//     show_admin_bar(false);
//     exit;
//   }
// }
function redirect_non_admin_users() {
  $user = wp_get_current_user();
  if( ( !defined('DOING_AJAX') || ! DOING_AJAX ) && ( empty( $user ) ||( !in_array( "administrator", (array) $user->roles ) && ! in_array('contributor', $user->roles ) ) ) ) {
    wp_safe_redirect(home_url());
    show_admin_bar(false);
    exit;
  }
}



add_action('after_setup_theme', 'remove_admin_bar');
 
function remove_admin_bar() {
  $user = wp_get_current_user();
  if ( ( in_array( 'administrator', $user->roles) || in_array('contributor', $user->roles) ) ) {
  } else {
    show_admin_bar(false);
  }
}

//Routes
Routes::map('announcements/page/:page', function($params) {
  $query = array(
    'post_type' => 'announcements',
    'paged' => $params['page']
  );
  Routes::load('archive-announcements.php', $params, $query);
});

Routes::map('category/pic/page/:page', function($params) {
  $query = array(
    'post_type' => 'post',
    'category_name' => 'pic',
    'paged' => $params['page']
  );
  Routes::load('archive-pic.php', $params, $query);
});

Routes::map('category/newsletter/page/:page', function($params) {
  $query = array(
    'post_type' => 'post',
    'category_name' => 'newsletter',
    'paged' => $params['page']
  );
  Routes::load('archive.php', $params, $query);
});

add_filter('the_content', 'remove_empty_p', 20, 1);
function remove_empty_p($content){
    $content = force_balance_tags($content);
    return preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content);
}

//Increase max size for file uploads
// @ini_set( 'upload_max_size' , '20M' );
// @ini_set( 'post_max_size', '20M');
// @ini_set( 'max_execution_time', '300' );
