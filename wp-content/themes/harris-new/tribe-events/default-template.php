<?php
/**
 * Default Events Template
 * This file is the basic wrapper template for all the views if 'Default Events Template'
 * is selected in Events -> Settings -> Display -> Events Template.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/default-template.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.23
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}


if( !is_user_logged_in() ) {
  header('Location: /');
}
$args = array (
  'post_type' => 'announcements',
  'meta_key' => 'featured',
  'meta_value' => true,
  'posts_per_page' => 1
);
$featured_announcements = get_posts( $args );
$featured_announcement = $featured_announcements[0];
// echo '<xmp>';
// print_r($featured_announcement);
// echo '</xmp>';
get_header();
?>
<main id="tribe-events-pg-template" class="tribe-events-pg-template">
  <?php 
    if(isset($featured_announcement)) {
      ?>
      <div>
      <div class="alert <?php echo $featured_announcement->colour;?>">
        <p class="alert-title"><?php echo $featured_announcement->post_title;?></p>
        <?php echo substr($featured_announcement->post_content, 0, 200) . '...';?>
        <a href="/announcements/<?php echo $featured_announcement->post_name;?>">Read full announcement</a>
      </div>
    </div>
  <?php
    }
  ?>
	<?php tribe_events_before_html(); ?>
	<?php tribe_get_view(); ?>
	<?php tribe_events_after_html(); ?>
</main> <!-- #tribe-events-pg-template -->
<?php
get_footer();
