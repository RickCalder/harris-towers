<?php
/* File: single.php */

if(! is_user_logged_in() ) {
  header('Location: /');
}
$context         = Timber::get_context();
$context['post'] = Timber::query_post();
if( is_user_logged_in() && ! is_page('announcements') ) {
  $args = array (
    'post_type' => 'announcements',
    'meta_key' => 'featured',
    'meta_value' => true,
    'posts_per_page' => 1
  );
  $featured_announcement = Timber::get_posts( $args );
  $context['featured_announcement'] = $featured_announcement[0];
}
Timber::render( 'pages/single.twig', $context );